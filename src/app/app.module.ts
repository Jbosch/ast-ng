import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor() {
    // #region INLINE function
    // const funcA = () => console.log('Development with inline func')
    // #endregion
    !environment.production ? this.funcA() : null;
  }

  // #region method on class
  private funcA() {
    console.log('Development method on class')
  }
  // #endregion

}
// #region method outside of class
// function funcA() {
//   console.log('Development method outside class')
// }
// #endregion
